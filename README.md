> **NOTE:** A README.md file should be placed at the **root of each of your repos directories.**
>


# LIS4369 - Extensible Enterprise Solutions

## Christopher Thompson

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    * Install Python
    * Install R
    * Install R studio
    * Install Visual Studio Code
    * Create a1_tip_calculator application
    * Provide screenshots of installations
    * Create Bitbucket repos
    * Complete Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command Descriptions
2. [A2 README.md](a2/README.md)
    * Must use float data type for user input
    * Overtime rate: 1.5 times hourly rate (hours over 40.)
    * Holiday rate: 2.0 times hourly rate (all holiday hours).
    * Must format currency with dollar sign, and round to two decimal places
3. [A3 README.md](a3/README.md)
    * Calculate home interior paint coat (w/o primer).
    * Must use float data types.
    * Must use SQFT_PER_GALLON constant (350)
    * Must use interation structure (aka "loop").
    * Format, right-align numbers, and round to two decima places.
4. [A4 README.md](a4/README.md)
    * Code and run demo.py.(Note: *be sure* necessary packages are installed!)
    * Then use it to backward-engineer the screenshots below it.
5. [A5 README.md](a5/README.md)
    * What is R? (and, download)
    * What is RStudio? (and, download)
    * R Commands: save a file of all the R commands included in the tutorial.
    * R Console: save a screenshot of some of the R commands executed above(below requirement). 
    * Graphs: save at least 5 separate image files displaying graph plots created from the tutorial.
    * RStudio: save one screenshot (similar to the one below), displaying the following 4 windows:
6. [P1 README.md](p1/README.md)
    * Run demo.py.
    * Test Python Package Installer: pip freeze
    * Installations: pandas, pandas-datare, andmatplotlib (only if missing)
7. [P2 README.md](p2/README.md)
    * Backward-engineer the lis4369_p2_requirements.txt file
    * Include at least two plots 
    * Use RStudio