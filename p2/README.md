> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4369 - Extensible Enterprise Solutions

## Chris Thompson

Project 2 Requirements:
3 Parts:

1.  Use Assignment 5 screenshots and R Manual to backward-engineer the 
    following requirements:
2.  Resources:
        a. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdf
        b. R for Data Science: https://r4ds.had.co.nz/
3.  Use Motor Trend Car Road Tests data:
        a. Research the data! https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/mtcars.html
        b. url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"
        Note: Use variable "mtcars" to read file into. (See Assignment 5 for reading .csv files.)

#### README.md file should include the following items:

* Screenshots of p2 code running
* git commands with short descriptionsr

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create a new local repository
2. git status - List the files you've changed and those you still need to add or commit
3. git add * or <filename> - Add one or more files to staging (index):
4. git remote -v - Verfify push location
5. git commit - Commit changes to head (but not yet to the remote repository)
6. git push - Send changes to the master branch of your remote repository:
7. git pull - Fetch and merge changes on the remote server to your working directory
8. git clone path/to/repository - Create a working copy of a local repository

#### Assignment Screenshots:

*Screenshot of plot 1 running in RStudio *:

![Graph 1 screenshot in RStudio code](img/Capture1.png)

*Screenshot of plot 2 runninig in RStudio *:

![Graph 2 screenshot in RStudio code](img/Capture2.png)

*Screenshot of some code output runninig in RStudio *:

![Graph 2 screenshot in RStudio code](img/Capture2_1.png)

*Screenshot of some code output running in RStudio *:

![Graph 2 screenshot in RStudio code](img/Capture3.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
