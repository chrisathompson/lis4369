> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4369 - Extensible Enterprise Solutions

## Chris Thompson

Assignment 1 Requirements:
Four Parts:

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions
4. Bitbucket repo links: a) this assigment and b) the completed tutorial (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshots of a1_tip_calculator application running
* git commands with short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create a new local repository
2. git status - List the files you've changed and those you still need to add or commit
3. git add * or <filename> - Add one or more files to staging (index):
4. git remote -v - Verfify push location
5. git commit - Commit changes to head (but not yet to the remote repository)
6. git push - Send changes to the master branch of your remote repository:
7. git pull - Fetch and merge changes on the remote server to your working directory
8. git clone path/to/repository - Create a working copy of a local repository

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator running in IDLE*:

![a1_tip_calculator screenshot in Idle](img/idle.png)

*Again *:  

*Screen shot a1 in visual studio code*:

![a1_tip_calculator in Visual Studio Code Screenshot](img/vsc.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
